/*!
 * TIM Developer: Israel Viveros
 *   Version: 3.0.12
 *   Copyright: Televisa Interactive Media (2014)
 */
;
(function() {
    $.fn.contadorHeader = function(parametros) {
        var setting = $.extend({
            'FechaTarget': new Date(),
            'linkButton': '#',
            'ticker': ''
        }, parametros);

        var globalThis = this;

        var CountDownHeader = {


            spanish: function() {
                $.countdown.regionalOptions['es'] = {
                    labels: ['A\u00F1os', 'Meses', 'Semanas', 'D\u00EDas', 'Hrs.', 'Min.', 'Seg.'],
                    labels1: ['A\u00F1o', 'Mes', 'Semana', 'D\u00EDas', 'Hrs.', 'Min.', 'Seg.'],
                    compactLabels: ['a', 'm', 's', 'g'],
                    whichLabels: null,
                    digits: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
                    timeSeparator: ':',
                    isRTL: false
                };
                $.countdown.setDefaults($.countdown.regionalOptions['es']);
            },

            startContador: function() {

                /*setInterval(function() {
                    CountDownHeader.buttonAside();
                }, 300000); // Actualizamos cada 5 min */

                var maquetado = "";
                maquetado += '<section class="wdg_contador_Header" style="display:none">';
                maquetado += '<div class="title_contador_header">Inicia en...</div>';
                maquetado += '<span class="countdown-row"  id="contador-Fase2y3">';
                maquetado += '<span class="countdown-section">';
                maquetado += '<span class="countdown-amount">000</span>';
                maquetado += '<span class="countdown-period">D\u00EDas</span>';
                maquetado += '</span>';
                maquetado += '<span class="countdown-section">';
                maquetado += '<span class="countdown-amount">000</span>';
                maquetado += '<span class="countdown-period">Hrs.</span>';
                maquetado += '</span>';
                maquetado += '<span class="countdown-section">';
                maquetado += '<span class="countdown-amount">000</span>';
                maquetado += '<span class="countdown-period">Min.</span>';
                maquetado += '</span>';
                maquetado += '<span class="countdown-section">';
                maquetado += '<span class="countdown-amount">000</span>';
                maquetado += '<span class="countdown-period">Seg.</span>';
                maquetado += '</span>';
                maquetado += '</span>';
                maquetado += '<div class="right_contador_header">';
                maquetado += '<div id="targetTIMCount"></div>';
                maquetado += '</section>';
                maquetado += '<div id="containerAsideCountdown"></div>';
                globalThis.empty().html(maquetado);
                //$("#VentasContaTIM").clone().appendTo("#targetTIMCount");


                CountDownHeader.spanish();
                //var tiempoMundial = setting.FechaTarget;
                var tiempoMundial = new Date(2014, 5, 12, 14, 0, 0, 0);
                globalThis.find("#contador-Fase2y3").countdown({
                    until: tiempoMundial,
                    format: 'DHMS',
                    padZeroes: true,
                    onExpiry: CountDownHeader.expire
                });

                var comprobando = globalThis.find("#contador-Fase2y3").countdown('getTimes');
                Array.prototype.contando = function(buscame) {
                    var i, count = 0;
                    for (i in this) {
                        if (Object.prototype.hasOwnProperty.call(this, i)) {
                            if (this[i] instanceof Array) count += this[i].contando(buscame);
                            else if (this[i] === buscame) count++;
                        }
                    }
                    return count;
                }

                if (comprobando.contando(0) === 7) {
                    setTimeout(function() {
                        CountDownHeader.expire();
                    }, 1000);
                } else {
                    globalThis.find(".wdg_contador_Header").fadeIn(1000);
                    //CountDownHeader.buttonAside();
                }


            },

            expire: function() {
                try {
                    CountDownHeader.horaServidor();

                    setInterval(function() {
                        CountDownHeader.horaServidor();
                    }, 60000); // Actualizamos cada 1 min 
                } catch (e) {
                    console.log(e);
                }
                //console.log("Se agoto el contador...");
            },


            horaServidor: function() {
                $.ajax({
                    url: "http://mxm.televisadeportes.esmas.com/deportes/home/timetvjsonp.js",
                    async: false,
                    cache: false,
                    dataType: 'jsonp',
                    jsonpCallback: 'timetv',
                    success: function(data) {
                        var arr = '';
                        var m = 0;
                        var anio = 0;

                        horas = data.timetv;
                        arr = data.fechatv.replace(/_/gi, "-").split("-");
                        m = parseInt(arr[1]) + 1;

                        if (String(m).length == 1) {
                            m = '0' + m;
                        }
                        anio = parseInt(arr[2]) + 1900;
                        fechas = m + '-' + arr[0] + '-' + anio;
                        fechas = fechas + ' ' + horas + ':00';

                        CountDownHeader.evalua(fechas);
                        CountDownHeader.nocache = horas.replace(":", "");

                    }
                });
            },

            evalua: function(now) {
                var arraytemp = new Array(),
                    arrayFecha = new Array(),
                    arrayHora = new Array();
                $.ajax({
                    url: 'http://static-televisadeportes.esmas.com/sportsdata/futbol/data/tickers/TickerFutbol_49.js',
                    type: 'GET',
                    dataType: 'jsonp',
                    cache: false,
                    jsonpCallback: 'wdata'
                })
                    .done(function(data) {
                        var fechaItem, horaItem, extras, fecha, hora, splitfecha, fechaAct, tmpM, tmpD, tmpY, horaAct, bandera = 0,
                            aH, hA, gg;

                        for (var i = 0; i < data.matches.match.length; i++) {
                            extras = String(data.matches.match[i].extras);
                            fechaItem = data.matches.match[i].MatchDate;
                            horaItem = data.matches.match[i].MatchHour;
                            // console.log("dia-> " + fechaItem + " HoraItem-> " + horaItem);
                            arraytemp.push(extras);
                            arrayFecha.push(fechaItem);
                            arrayHora.push(horaItem);
                        };
                        //console.log(arraytemp);
                        splitfecha = now.split(" ");
                        fechaAct = new Date(splitfecha[0]).getTime();
                        horaAct = splitfecha[1];

                        for (var x = 0; x < arrayFecha.length; x++) {
                            var tmpF = arrayFecha[x].split("/");
                            tmpM = parseInt(tmpF[1]) - 1;
                            tmpD = tmpF[0];
                            tmpY = tmpF[2];
                            //console.log("comparando: " + fechaAct + ' -- ' + new Date(tmpY, tmpM, tmpD).getTime())
                            if (fechaAct === new Date(tmpY, tmpM, tmpD).getTime()) {
                                //console.log("son fechas Iguales")
                                aH = String(arrayHora[x]).split(":");
                                hA = String(horaAct).split(":");
                                gg = parseInt(aH[0]) + 2;
                                if (parseInt(hA[0]) >= parseInt(aH[0]) && parseInt(hA[0]) < gg) {
                                    bandera = x;
                                }

                            }
                        };
                        var banderaDraw = 0;
                        console.log(arraytemp);
                        //FOR EN VIVO
                        //console.log("FOR EN VIVO");
                        for (var i = 0; i < arraytemp.length; i++) {
                            //console.log(arraytemp[i]);
                            if (arraytemp[i].indexOf("vivo") !== -1 && !$("[data-wdgtipo=vivo]").length) {
                                CountDownHeader.creacontenido("vivo", data.matches.match[i]);
                                //console.log("estamos en vivo");
                                i = 2000;
                                banderaDraw = 1;
                            }
                        };
                        if ($("[data-wdgtipo=vivo]").length) {
                            banderaDraw = 1;
                        }
                        //FOR PROXIMO
                        if (banderaDraw === 0) {
                            //console.log("FOR EN PROXIMO");
                            for (var i = 0; i < arraytemp.length; i++) {
                                //console.log(arraytemp[i]);
                                if (arraytemp[i].indexOf("proximo") !== -1 && !$("[data-wdgtipo=proximo]").length) {
                                    CountDownHeader.creacontenido("proximo", data.matches.match[i]);
                                    //console.log("en proximo")
                                    i = 2000;
                                    banderaDraw = 1;
                                }
                            };
                        }
                        if ($("[data-wdgtipo=proximo]").length) {
                            banderaDraw = 1;
                        }
                        //FOR CONECTA TD
                        if (banderaDraw === 0) {
                            //console.log("FOR EN CONECTA TD");
                            for (var i = 0; i < arraytemp.length; i++) {
                                //console.log(arraytemp[i]);
                                if (arraytemp[i].indexOf("conecta") !== -1 && !$("[data-wdgtipo=conecta]").length) {
                                    CountDownHeader.creacontenido("conecta", data.matches.match[i]);
                                    //console.log("en previo");
                                    i = 2000;
                                    banderaDraw = 1;
                                }
                            };
                        }
                        if ($("[data-wdgtipo=conecta]").length) {
                            banderaDraw = 1;
                        }
                        //FOR PREVIO
                        if (banderaDraw === 0) {
                            //console.log("FOR EN PREVIO");
                            for (var i = 0; i < arraytemp.length; i++) {
                                //console.log(arraytemp[i]);
                                if (arraytemp[i].indexOf("previo") !== -1 && !$("[data-wdgtipo=previo]").length) {
                                    CountDownHeader.creacontenido("previo", data.matches.match[i]);
                                    //console.log("en previo");
                                    i = 2000;
                                    banderaDraw = 1;
                                }
                            };
                        }
                        if ($("[data-wdgtipo=previo]").length) {
                            banderaDraw = 1;
                        }
                        //FOR REVIVE
                        if (banderaDraw === 0) {
                            //console.log("FOR EN REVIVE");
                            for (var i = 0; i < arraytemp.length; i++) {
                                //console.log(arraytemp[i]);
                                if (arraytemp[i].indexOf("revive") !== -1 && !$("[data-wdgtipo=revive]").length) {
                                    CountDownHeader.creacontenido("revive", data.matches.match[i]);
                                    //console.log("en revive");
                                    i = 2000;
                                    banderaDraw = 1;
                                }
                            };
                        }
                        if ($("[data-wdgtipo=revive]").length) {
                            banderaDraw = 1;
                        }


                        if (banderaDraw === 0) {
                            console.log("no hay ningun caso");
                            //console.log("hago el ultimo");
                            var ultimo = parseInt(data.matches.match.length) - 1;
                            CountDownHeader.creacontenido("revive", data.matches.match[ultimo]);
                        }


                    })
                    .fail(function() {
                        console.log("Error al cargar el ticker: " + setting.ticker);
                    })



            },

            buttonAside: function() {
                $.ajax({
                    url: 'http://televisadeportes.esmas.com/copa-mundial-fifa-brasil-2014/configbutton.html',
                    type: 'GET',
                    dataType: 'jsonp',
                    jsonpCallback: 'asidecount',
                    cache: true,
                    data: CountDownHeader.nocache
                })
                    .done(function(data) {
                        var interaccionData = '<a target="_blank" href="' + data.link + '" class="ui-link"><div id="contTIMinteraccion" class="wdg_contador_aside"><img src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/hand_icon.png"><span>PARTICIPA EN </span><span>INTERACCI\u00D3N TD</span></div></a>';
                        var lajugada = '<a target="_blank" href="' + data.link + '" class="ui-link"><div id="contTIMlajugada" class="wdg_contador_aside_revive"><span>INTERACT\u00DAA CON</span><div class="line"></div><img src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/logolajugada.png"></div></a>';
                        if (!$("#contTIM" + data.tipo).length) {
                            switch (data.tipo) {
                                case "lajugada":
                                    $("#containerAsideCountdown").css("display", "none").html(lajugada).fadeIn('fast', function() {
                                        $(this).css('display', 'block');
                                    });
                                    break;
                                case "interaccion":
                                    $("#containerAsideCountdown").css("display", "none").html(interaccionData).fadeIn('fast', function() {
                                        $(this).css('display', 'block');
                                    });
                                    break;
                                default:
                                    $("#containerAsideCountdown").css('display', 'none');
                                    break;
                            }

                        }
                    })
                    .fail(function() {
                        console.log("error al cargar feed aside");
                    })

            },

            creacontenido: function(type, data) {
                var maquetado = "",
                    numForlocal = 0,
                    numForVisit = 0;
                $.ajax({
                    //url: 'http://static-televisadeportes.esmas.com/sportsdata/futbol/copa-mundial-fifa-brasil-2014/teams.jsonp',
                    url: 'http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/js/teams.jsonp',
                    type: 'GET',
                    dataType: 'jsonp',
                    jsonpCallback: 'getTeams'
                })
                    .done(function(banderas) {
                        //var paisActual1 = data.equipos.local.name.toLowerCase().replace(/á/g,"\u00e1").replace(/é/g,"\u00e9").replace(/í/g,"\u00ed").replace(/ó/g,"\u00f3").replace(/ú/g,"\u00fa").replace(/ñ/g,"\u00f1");
                        //var paisActual2 = data.equipos.visit.name.toLowerCase().replace(/á/g,"\u00e1").replace(/é/g,"\u00e9").replace(/í/g,"\u00ed").replace(/ó/g,"\u00f3").replace(/ú/g,"\u00fa").replace(/ñ/g,"\u00f1");
                        var paisActual1 = data.equipos.local.name.toLowerCase();
                        var paisActual2 = data.equipos.visit.name.toLowerCase();

                        for (var i = 0; i < banderas.teams.length; i++) {
                            //console.log("comparando: "+paisActual1 +" - "+banderas.teams[i].nombre.toLowerCase());
                            if (paisActual1 === banderas.teams[i].nombre.toLowerCase()) {
                                //console.log("----");
                                numForlocal = i;
                            }
                            //console.log("comparando: "+paisActual2 +" - "+banderas.teams[i].nombre.toLowerCase());
                            if (paisActual2 === banderas.teams[i].nombre.toLowerCase()) {
                                //console.log("----");
                                numForVisit = i;
                            }
                        };

                        switch (type) {
                            case "vivo":
                                //console.log("VIVO");
                                var urlVivow = (data.EventUrl !== "") ? data.EventUrl : data.Website + 'mxm.html';
                                maquetado += (data.EventUrl !== "") ? '<section class="wdg_contador_Header_vivo givelinkAll" rel="' + urlVivow + '" style="display:none;cursor:pointer" data-wdgtipo="vivo">' : '<section class="wdg_contador_Header_vivo givelinkAll mxm_novideo" rel="' + urlVivow + '" style="display:none;cursor:pointer" data-wdgtipo="vivo">';
                                maquetado += (data.EventUrl !== "") ? '<a class="ui-link"><img src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/camara2.png"></a>' : '<a></a>';
                                maquetado += (data.EventUrl !== "") ? '<span class="separador">|</span>' : '<span></span>';
                                maquetado += (data.EventUrl !== "") ? '<div class="title_contador_header_vivo animafade"><a>VER EN VIVO AHORA</a>' : '<div class="title_contador_header_vivo mxm_status animafade"><a>MINUTO A MINUTO</a>';
                                maquetado += (data.EventUrl !== "") ? '<img src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/arrow2.png">' : '<img src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/arrow3.png">';
                                maquetado += '</div>';
                                maquetado += '<div class="right_contador_header_vivo">';
                                maquetado += '<div class="wdg_paises">';
                                maquetado += '<div rel="' + banderas.teams[numForlocal].url + '" style="cursor:pointer">';
                                maquetado += (numForlocal >= 0) ? banderas.teams[numForlocal].alias : data.equipos.local.name.substring(0, 3);
                                maquetado += (numForlocal >= 0) ? '<img src="' + banderas.teams[numForlocal].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += '</div>';
                                maquetado += '<div class="vs_wdg_paises">VS</div>';
                                maquetado += '<div rel="' + banderas.teams[numForVisit].url + '" style="cursor:pointer">';
                                maquetado += (numForVisit >= 0) ? '<img src="' + banderas.teams[numForVisit].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += (numForVisit >= 0) ? banderas.teams[numForVisit].alias : data.equipos.visit.name.substring(0, 3);
                                maquetado += '</div>';
                                maquetado += '</div>';
                                maquetado += '<div id="targetTIMCount"></div>';
                                maquetado += '</div>';
                                maquetado += '</section>';
                                maquetado += '<div id="containerAsideCountdown"></div>';

                                break;
                            case "proximo":
                                //console.log("PROXIMO")
                                var mesNum = parseInt(data.MatchDate.substring(3, 5)),
                                    nameMes = "";
                                switch (mesNum) {
                                    case 1:
                                        nameMes = "Ene.";
                                        break;
                                    case 2:
                                        nameMes = "Feb.";
                                        break;
                                    case 3:
                                        nameMes = "Mar.";
                                        break;
                                    case 4:
                                        nameMes = "Abr.";
                                        break;
                                    case 5:
                                        nameMes = "May.";
                                        break;
                                    case 6:
                                        nameMes = "Jun.";
                                        break;
                                    case 7:
                                        nameMes = "Jul.";
                                        break;
                                    case 8:
                                        nameMes = "Ago.";
                                        break;
                                    case 9:
                                        nameMes = "Sep.";
                                        break;
                                    case 10:
                                        nameMes = "Oct.";
                                        break;
                                    case 11:
                                        nameMes = "Nov.";
                                        break;
                                    case 12:
                                        nameMes = "Dic.";
                                        break;
                                    default:
                                        nameMes = mesNum;
                                        break;
                                }
                                maquetado += '<section class="wdg_contador_Header_proximo givelinkAll" style="display:none;cursor:pointer;" rel="' + data.Website + '" data-wdgtipo="proximo">';
                                maquetado += '<img src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/camara3.png" width="22" height="13">';
                                maquetado += '<span class="separador_proximo">|</span>';
                                maquetado += '<div class="title_contador_header_proximo"><a>Pr\u00F3ximo partido</a>';
                                maquetado += '</div>';
                                maquetado += '<div class="right_contador_header_proximo">';
                                maquetado += '<div class="wdg_paises">';
                                maquetado += '<div rel="' + banderas.teams[numForlocal].url + '" style="cursor:pointer">';
                                maquetado += (numForlocal >= 0) ? banderas.teams[numForlocal].alias : data.equipos.local.name.substring(0, 3);
                                maquetado += (numForlocal >= 0) ? '<img src="' + banderas.teams[numForlocal].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += '</div>';
                                maquetado += '<div class="vs_wdg_paises">VS</div>';
                                maquetado += '<div rel="' + banderas.teams[numForVisit].url + '" style="cursor:pointer">';
                                maquetado += (numForVisit >= 0) ? '<img src="' + banderas.teams[numForVisit].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += (numForVisit >= 0) ? banderas.teams[numForVisit].alias : data.equipos.visit.name.substring(0, 3);
                                maquetado += '</div>';
                                maquetado += '</div>';
                                maquetado += '<div id="targetTIMCount"></div>';
                                maquetado += '</div>';
                                maquetado += '<span class="countdown-section_proximo" id="contador-Fase2y3">';
                                maquetado += '<span class="fecha">' + data.MatchDate.substring(0, 2) + ' ' + nameMes + ' ' + data.MatchHour.substring(0, 6) + ' hrs</span>';
                                maquetado += '</span>';
                                maquetado += '</section>';
                                maquetado += '<div id="containerAsideCountdown"></div>';

                                break;
                            case "previo":
                                //console.log("PREVIO");
                                maquetado += '<section class="wdg_contador_Header_conecta previo_solo givelinkAll" style="display:none;cursor:pointer;" rel="' + data.Website + '" data-wdgtipo="previo">';
                                maquetado += '<div class="title_contador_header_conecta"><a>PREVIO</a></div>';
                                maquetado += '<a><img class="imgconect" src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/arrow2.png"></a>';
                                maquetado += '<div class="right_contador_header_conecta">';
                                maquetado += '<div class="wdg_paises">';
                                maquetado += '<div rel="' + banderas.teams[numForlocal].url + '" style="cursor:pointer">';
                                maquetado += (numForlocal >= 0) ? banderas.teams[numForlocal].alias : data.equipos.local.name.substring(0, 3);
                                maquetado += (numForlocal >= 0) ? '<img src="' + banderas.teams[numForlocal].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += '</div>';
                                maquetado += '<div class="vs_wdg_paises">VS</div>';
                                maquetado += '<div rel="' + banderas.teams[numForVisit].url + '" style="cursor:pointer">';
                                maquetado += (numForVisit >= 0) ? '<img src="' + banderas.teams[numForVisit].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += (numForVisit >= 0) ? banderas.teams[numForVisit].alias : data.equipos.visit.name.substring(0, 3);
                                maquetado += '</div>';
                                maquetado += '</div>';
                                maquetado += '<div id="targetTIMCount"></div>';
                                maquetado += '</div>';
                                maquetado += '</section>';
                                maquetado += '<div id="containerAsideCountdown"></div>';
                                break;
                            case "conecta":
                                //console.log("CONECTA");
                                var linkweb2 = (typeof data.EventUrl !== "undefined" && data.EventUrl !== "") ? data.EventUrl : data.Website;
                                maquetado += '<section class="wdg_contador_Header_conecta conecta_solo givelinkAll" style="display:none;cursor:pointer;" rel="' + linkweb2 + '" data-wdgtipo="conecta">';
                                maquetado += '<div class="title_contador_header_conecta"><a>PREVIO</a></div>';
                                maquetado += '<a><img class="imgconect" src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/logoconecta_td.png"></a>';
                                maquetado += '<div class="right_contador_header_conecta">';
                                maquetado += '<div class="wdg_paises">';
                                maquetado += '<div rel="' + banderas.teams[numForlocal].url + '" style="cursor:pointer">';
                                maquetado += (numForlocal >= 0) ? banderas.teams[numForlocal].alias : data.equipos.local.name.substring(0, 3);
                                maquetado += (numForlocal >= 0) ? '<img src="' + banderas.teams[numForlocal].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += '</div>';
                                maquetado += '<div class="vs_wdg_paises">VS</div>';
                                maquetado += '<div rel="' + banderas.teams[numForVisit].url + '" style="cursor:pointer">';
                                maquetado += (numForVisit >= 0) ? '<img src="' + banderas.teams[numForVisit].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += (numForVisit >= 0) ? banderas.teams[numForVisit].alias : data.equipos.visit.name.substring(0, 3);
                                maquetado += '</div>';
                                maquetado += '</div>';
                                maquetado += '<div id="targetTIMCount"></div>';
                                maquetado += '</div>';
                                maquetado += '</section>';
                                maquetado += '<div id="containerAsideCountdown"></div>';
                                break;
                            case "revive":
                                //console.log("REVIVE");
                                var linkweb = (typeof data.EventUrl !== "undefined" && data.EventUrl !== "") ? data.EventUrl : data.Website;
                                maquetado += '<section class="wdg_contador_Header_revive givelinkAll" style="display:none;cursor:pointer;" rel="' + linkweb + '" data-wdgtipo="revive">';
                                maquetado += '<img src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/camara3.png" class="imgfx" width="22" height="13">';
                                maquetado += '<span class="separador_revive">|</span>';
                                maquetado += '<div class="title_contador_header_revive"><a>REVIVE EL PARTIDO</a>';
                                maquetado += '<img src="http://i2.esmas.com/deportes30/copa-mundial-fifa-brasil-2014/Fase2yFase3/img/arrow3.png">';
                                maquetado += '</div>';
                                maquetado += '<div class="right_contador_header_revive">';
                                maquetado += '<div class="wdg_paises">';
                                maquetado += '<div rel="' + banderas.teams[numForlocal].url + '" style="cursor:pointer">';
                                maquetado += (numForlocal >= 0) ? banderas.teams[numForlocal].alias : data.equipos.local.name.substring(0, 3);
                                maquetado += (numForlocal >= 0) ? '<img src="' + banderas.teams[numForlocal].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgbanderaCon">';
                                maquetado += '</div>';
                                maquetado += '<div class="vs_wdg_paises">VS</div>';
                                maquetado += '<div rel="' + banderas.teams[numForVisit].url + '" style="cursor:pointer">';
                                maquetado += (numForVisit >= 0) ? '<img src="' + banderas.teams[numForVisit].bandera + '" class="imgbanderaCon">' : '<img src="http://i2.esmas.com/img/spacer.gif" class="imgfxbanderaCon">';
                                maquetado += (numForVisit >= 0) ? banderas.teams[numForVisit].alias : data.equipos.visit.name.substring(0, 3);
                                maquetado += '<div id="targetTIMCount"></div>';
                                maquetado += '</div>';
                                maquetado += '</section>';
                                maquetado += '<div id="containerAsideCountdown"></div>';
                                break;
                        }


                        banderas.teams[numForlocal].url

                        globalThis.html(maquetado).children().fadeIn('slow', function() {
                            CountDownHeader.buttonAside();
                        });


                        /*$(document).on("click", '.givelocallink,.givevisitlink', function() {
                            window.open($(this).attr('rel'), '_blank');
                        });*/
                        $(document).on("click", '.givelinkAll', function() {
                            $(this).css('cursor', 'pointer');
                            window.open($(this).attr('rel'), '_blank');
                        });

                        if ($(".animafade").length) {
                            $(".animafade").bind("animation.loop", function() {
                                $(this).fadeIn(1500, function() {
                                    $(this).delay(8000).fadeOut(1500, function() {
                                        $(this).css('display', 'none').trigger("animation.loop");
                                    });
                                });
                            }).trigger("animation.loop");
                        }

                    })
                    .fail(function() {
                        console.log("error al cargar las banderas");
                    })



            }



        }

        CountDownHeader.startContador();

    }
})(jQuery);